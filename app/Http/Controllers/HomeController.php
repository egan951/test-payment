<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\KaryawanModel;
use Illuminate\Support\Str;

class HomeController extends Controller
{   


    public function index() {

        $data_pekerjaan = DB::table('data_karyawan')
        ->select('pekerjaan')
        ->groupBy('pekerjaan')
        ->orderBy('pekerjaan')
        ->get();
        $data = [
            'pekerjaan' => $data_pekerjaan
        ];
        return view("v_home",$data);
    }

    public function getData(Request $request) {
        $pekerjaan = $request->input('pekerjaan');

        $this->start = $request->input('start');
        $data = DB::table('data_karyawan as a')
        ->select('a.id','a.nama', 'a.pekerjaan','a.tanggal_lahir');
        if($pekerjaan!='') {
            $data->where('pekerjaan',$pekerjaan);
        }
        return Datatables::of($data)
        ->smart(false)
        ->filter(function ($query) {
            $search = request('search');
            $this->val_search = $search['value'];
            if ($this->val_search!='') {
                $query->Where(function($query1) {
                    $query1->Where('a.nama', 'like', "%" . $this->val_search . "%")
                    ->orWhere('a.pekerjaan', 'like', "%" . $this->val_search . "%");
                });
            }
        })
        ->addColumn('aksi', function($row){
            $btn = '<a href="ajavascript:;" onclick="formKaryawan(\''.$row->id.'\',\'update\')" data-uid = "'.$row->id.'" class="edit_data btn btn-info btn-sm">Update</a> <a href="ajavascript:;" data-uid = "'.$row->id.'" data-nama = "'.$row->nama.'" class="hapus_data btn btn-danger btn-sm">Hapus</a>';
            return $btn;
        })
        ->addColumn('no', function($row){
            return ++$this->start;
        })
        ->rawColumns(['aksi','no'])
     ->toJson();
    }

    public function getKaryawanForm(Request $request) {
        $aksi = $request->input('aksi');
        $uid = $request->input('uid');

        $nama = '';
        $pekerjaan = '';
        $tanggal_lahir = '';

        $status = 'bad';
        if($aksi=='update') {
            if($uid!='') {
                $data_karyawan = KaryawanModel::where('id', $uid)->first();
                $nama = $data_karyawan->nama;
                $pekerjaan = $data_karyawan->pekerjaan;
                $tanggal_lahir = $data_karyawan->tanggal_lahir;
                $status = 'ok';
            }
        }

        $data = [
            'nama' => $nama,
            'pekerjaan' => $pekerjaan,
            'tanggal_lahir' => $tanggal_lahir
        ];
        return response()->json(['status'=> $status, 'data'=>$data]);
    }

    public function simpanKaryawan(Request $request) {
        $request->validate([
            'nama' => 'required',
            'pekerjaan' => 'required',
            'tanggal_lahir' => 'required'
        ]);

        $aksi = $request->input('aksi');
        $uid = $request->input('uid');
        $nama = $request->input('nama');
        $pekerjaan = $request->input('pekerjaan');
        $tanggal_lahir = $request->input('tanggal_lahir');

        $status = 0;
        $message = 'Invalid Action!!';
        if($aksi=='update') {
            if($uid!='') {
                $data_to_update = [
                    'nama' => $nama,
                    'pekerjaan' => $pekerjaan,
                    'tanggal_lahir' => $tanggal_lahir
                ];
                KaryawanModel::where('id',$uid)->update($data_to_update);
                $status = 1;
                $message ="Data karyawan berhasil diupdate";
            }
        } else if($aksi=='new') {
            $data_to_insert = [
                'id' =>Str::uuid(),
                'nama' => $nama,
                'pekerjaan' => $pekerjaan,
                'tanggal_lahir' => $tanggal_lahir
            ];
            KaryawanModel::create($data_to_insert);
            $status = 1;
            $message ="Data karyawan berhasil disimpan";
        }

        return response()->json(['status'=> $status, 'message'=>$message]);
    }

    public function hapusData(Request $request) {
        $uid = $request->input('uid');
        if($uid!='') {
            $deletedRows = KaryawanModel::where('id', $uid)->delete();
            if($deletedRows==0) {
                $status = 2;
                $message ="Tidak ada data yang dihapus";
            } else {
                $status = 1;
                $message ="Data berhasil dihapus";
            }
        } else {
            $status = 2;
            $message ="Data tidak ditemukan";
        }
        return response()->json(['status'=> $status, 'message'=>$message]);
    }

    public function demo_payment() {


        $data = [

        ];
        return view("v_demo_payment",$data);
    }

    public function link_payment(Request $request) {
        $va           = '0000007755506267'; 
        $secret       = 'SANDBOXEE5E1832-9838-44A2-A94C-5B54E8629F68-20210915120759';

        $url          = 'https://sandbox.ipaymu.com/api/v2/payment';
        $method       = 'POST';

        //Request Body//
        $body['product']    = array('Seduh Kopi');
        $body['qty']        = array('1');
        $body['price']      = array('550000');
        $body['returnUrl']  = url('home/demo-payment');
        $body['cancelUrl']  = url('home/demo-payment');
        $body['notifyUrl']  = url('home/notify-payment');
        $body['buyer_name'] = 'I Kadek Jaya';
        $body['buyer_email'] = 'eganjaya75@gmail.com';
        //End Request Body//

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature


        $ch = curl_init($url);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'va: ' . $va,
            'signature: ' . $signature,
            'timestamp: ' . $timestamp
        );

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, count($body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $err = curl_error($ch);
        $ret = curl_exec($ch);
        curl_close($ch);
        if($err) {
            echo $err;
        } else {

            //Response
            $ret = json_decode($ret);
            print_r($ret);
            if($ret->Status == 200) {
                $sessionId  = $ret->Data->SessionID;
                $url        =  $ret->Data->Url;
                return redirect($url);
            } else {
                echo $ret;
            }
            //End Response
        }
    }

}