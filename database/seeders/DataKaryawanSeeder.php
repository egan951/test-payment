<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class DataKaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for($i = 1; $i <= 50; $i++){
            DB::table('data_karyawan')->insert([
                'id' => Str::uuid(),
                'nama' => $faker->name,
                'pekerjaan' => $faker->jobTitle,
                'tanggal_lahir' => $faker->date('Y-m-d', '2000-12-31')
            ]);
        }
        
    }
}
