# Payment Test

Fullstack test by I Kadek Meganjaya

# Project Overview

Project dibuat dengan **Laravel 8 framework** dan membutuhkan server dengan spesifikasi minimum seperti dibawah ini:

- PHP 7.3+

- mysql 5.6+

Dalam project ini, Terdapat fitur CRUD dan filter data pada menu **home** dan test redirect payment menggunakan payment gateway ipaymu pada menu **Demo Payment**

# Konfigurasi
- Jalankan perintah `composer update` untuk mendownload semua package
- Generate key dengan perintah `php artisan key:generate`
- Jangan lupa ubah nama file env.example menjadi .env dan atur konfigurasi seperti database, app name dll sesuai dengan konfigurasi yang diinginkan

# Database migration & Seed

- Ketik `php artisan migrate` untuk memulai migrasi atau import tabel yang sudah ada.

- Ketik `php artisan db:seed --class=DataKaryawanSeeder` untuk menginputkan data random sebanyak 50 data.

# Menjalankan Project

Ketik `php artisan serve` pada cmd (windows) atau terminal(linux), atau kamu dapat membuat virtualhost dan arahkan ke document root pada project kamu