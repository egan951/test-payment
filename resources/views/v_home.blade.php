<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home | Payment Test</title>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.2/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Test Payment</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="{{url(route('home'))}}">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('home/demo-payment')}}">Demo Payment</a>
              </li>
            </ul>

          </div>
        </div>
      </nav>
    <div class="container mt-5">
        <div class="row d-flex justify-content-center mb-3">
            <div class="col-md-4">
                <label for="filter-pekerjaan" class="control-label">Filter Pekerjaan:</label>
                <select class="select2 form-control" name="filter_pekerjaan" id="filter_pekerjaan">
                    <option value="">--Semua Pekerjaan--</option>
                    @foreach ($pekerjaan as $item)
                        <option value="{{$item->pekerjaan}}">{{$item->pekerjaan}}</option> 
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-2">
                <button onclick="formKaryawan()" type="button" id="tambah_data" class="btn btn-primary">Tambah Data</button>
            </div>
        </div>
        <table id="tabel_karyawan" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Pekerjaan</th>
                    <th>Tanggal Lahir</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- Modal -->
    <div id="modal_karyawan" class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal_karyawan" aria-hidden="true">
        <form id="f_karyawan">
            <input type="hidden" id="uid" name="uid" value="">
            <input type="hidden" id="aksi" name="aksi" value="new">
            @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama" aria-describedby="msg_nama" required>
                                <div id="msg_nama" class="form-text text-danger"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="pekerjaan" class="form-label">Pekerjaan</label>
                                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" aria-describedby="msg_pekerjaan" required>
                                <div id="msg_pekerjaan" class="form-text text-danger"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                                <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" aria-describedby="msg_tanggal_lahir" required>
                                <div id="msg_tanggal_lahir" class="form-text text-danger"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" id="btn_simpan" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.2/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/id.min.js" integrity="sha512-he8U4ic6kf3kustvJfiERUpojM8barHoz0WYpAUDWQVn61efpm3aVAD8RWL8OloaDDzMZ1gZiubF9OSdYBqHfQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        let tabel_karyawan;
        let tanggal_lahir = $("#tanggal_lahir");
        function formKaryawan(uid='',aksi='new') {
            $('#modal_karyawan .modal-title').html('Tambah Karyawan');
            $('#modal_karyawan').modal('show');
            if(uid!='' && aksi=='update') {
                $('#modal_karyawan .modal-title').html('Edit Karyawan');
                $('#modal_karyawan #aksi').val('update');
                $('#modal_karyawan #uid').val(uid);
                $.ajax({
                    type : 'POST',
                    url : "{{url('home/get-karyawan-form')}}",
                    data : {
                        'uid':uid,
                        'aksi':aksi,
                        '_token' :  "{{ csrf_token() }}",
                    },
                    cache : false
                })
                .done(function(response) {
                    $('#modal_karyawan #nama').val(response.data.nama);
                    $('#modal_karyawan #pekerjaan').val(response.data.pekerjaan);
                    tanggal_lahir.flatpickr(
                        {
                            altInput: true,
                            altFormat: "d M Y",
                            dateFormat: "Y-m-d",
                            defaultDate: response.data.tanggal_lahir
                        }
                    );
                    //$('#modal_karyawan #tanggal_lahir').val(response.data.tanggal_lahir);
                })
                .fail(function(x, status, error) {
                    swal.fire({
                        text: "An error occurred: " + x.status + ", nError: " + error,
                        icon: "error",
                        confirmButtonText: "Ok!",
                        allowOutsideClick: false
                    }).then(function(result) {
                        if (result.value) {
                            $('#modal_karyawan').modal('hide');
                        }
                    })
                })
            } 

        }
        
        function showSwalLoading(text) {
            Swal.fire({
                title: text,
                allowOutsideClick: false,
                didOpen: function() {
                    Swal.showLoading()
                }
            })
        }
        $(document).ready(function() {
            $('#modal_karyawan').on('hidden.bs.modal', function (event) {
                $("#f_karyawan").trigger("reset");
                $('.form-text').html('');
            })

            $('.select2').select2();

            tabel_karyawan = $('#tabel_karyawan').DataTable( {
            "processing": true,
            "serverSide": true,
            "order":[],
            "ajax": {
                "url": "{{url('home/get-data')}}",
                "type": "POST",
                "data": function ( d ) {
                    d._token =  "{{ csrf_token() }}";
                    d.pekerjaan = $('#filter_pekerjaan').val();
                }

            },
            "columns": [
                { "data": "no"},
                { "data": "nama"},
                { "data": "pekerjaan"},
                { "data": "tanggal_lahir"},
                { "data": null},
                { "data": null }
            ],
            "columnDefs": [ 
                { "targets": 0,"searchable": false, "orderable":false},
                { "targets": 3,"searchable": false, "orderable":true,
                    render: function ( data, type, row ) {
                        return moment(row.tanggal_lahir).format('DD MMM YYYY');
                    } 
                },
                { "targets": 4,"searchable": false, "orderable":false,
                    render: function ( data, type, row ) {
                        var date = moment(row.tanggal_lahir).format('D');
                        var week = moment(row.tanggal_lahir);
                        week = week.week() - moment(week).startOf('month').week() + 1;

                        var date_status = "Tanggal Ganjil";
                        var date_bg = "primary";
                        if((date%2)==0) {
                            date_status = "Tanggal Genap";
                            date_bg = "success";
                        }

                        var week_status = "Minggu Ganjil";
                        var week_bg = "primary";
                        if((week%2)==0) {
                            week_status = "Minggu Genap";
                            week_bg = "success";
                        }

                        return `
                            <span class="badge bg-`+date_bg+`">`+date_status+`</span><br>
                            <span class="badge bg-`+week_bg+`">`+week_status+`</span><br>
                        `;
                    } 
                
                },
                { "targets": 5,"searchable": false, "orderable":false,
                render: function ( data, type, row ) {
                    return row.aksi;
                    }
                } 
            ]

        } );

        $('#filter_pekerjaan').on('change',function() {
            tabel_karyawan.ajax.reload();
        })

        tanggal_lahir.flatpickr(
            {
                altInput: true,
                altFormat: "d M Y",
                dateFormat: "Y-m-d"
            }
        );
        

        $('#f_karyawan').submit(function(evt){
            evt.preventDefault();
            var btn = $("#btn_simpan");
            btn.prop('disabled',true);
            btn.text('Menyimpan...');
            let formData = new FormData(this);
            $.ajax({
                type : 'POST',
                url : '{{url("home/simpan-karyawan")}}',
                data : formData,
                cache : false,
                contentType : false,
                processData : false,
                dataType:'json'
            })
            .done(function(response) {
                swal.fire({
                    text: response.message,
                    icon: "success",
                    confirmButtonText: "Ok!",
                    allowOutsideClick: false
                }).then(function(result) {
                    if (result.value) {
                        tabel_karyawan.ajax.reload();
                        btn.prop('disabled',false);
                        btn.text('Simpan');
                        $('#modal_karyawan').modal('hide');
                    }
                })
               
            })
            .fail(function(x, status, error) {
                if(x.status==422) {
                    $('.form-text').html('');
                    $.each(x.responseJSON.errors, function( index, value ) {
                        $('#msg_'+index).html(value);
                    });
                } else {
                    swal.fire({
                        text: "An error occurred: " + x.status + ", nError: " + error,
                        icon: "error",
                        confirmButtonText: "Ok!"
                    })
                }
                btn.prop('disabled',false);
                btn.text('Simpan');
                
            })
        });

        $(document).on('click','.hapus_data',function(){
            let uid = $(this).data('uid');
            let nama = $(this).data('nama');
            Swal.fire({
                title: "Apakah anda yakin?",
                html: "Karyawan <b>"+nama+"</b> akan dihapus!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    showSwalLoading('Menghapus Data...')
                    $.ajax({
                        type : 'POST',
                        url : "{{url('home/hapus-data')}}",
                        data : {
                            'uid':uid,
                            '_token' :  "{{ csrf_token() }}",
                        },
                        cache : false,
                        dataType:'json'
                    })
                    .done(function(response) {
                        if(response.status==1) {
                            swal.fire({
                                text: response.message,
                                icon: "success",
                                confirmButtonText: "Ok!",
                                allowOutsideClick: false
                            }).then(function(result) {
                                if (result.value) {
                                    tabel_karyawan.ajax.reload();
                                }
                            })
                        } else {
                            swal.fire({
                                text: response.message,
                                icon: "success",
                                confirmButtonText: "Ok!",
                                allowOutsideClick: false
                            })
                        }
                    })
                    .fail(function(x, status, error) {
                        swal.fire({
                            text: "An error occurred: " + x.status + ", nError: " + error,
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok!",
                            allowOutsideClick: false,
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        })
                    })
                    
                }
            });
        })

        })
    </script>
</body>
</html>