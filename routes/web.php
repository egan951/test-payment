<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'index'])->name('home');
Route::post('/home/get-data',[HomeController::class, 'getData']);
Route::post('/home/get-karyawan-form',[HomeController::class, 'getKaryawanForm']);
Route::post('/home/simpan-karyawan',[HomeController::class, 'simpanKaryawan']);
Route::post('/home/hapus-data',[HomeController::class, 'hapusData']);
Route::get('/home/demo-payment',[HomeController::class, 'demo_payment']);
Route::get('/home/link-payment',[HomeController::class, 'link_payment']);
